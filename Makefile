APP_NAME = wm_helper

VERSION:=$(shell grep ^version: shard.yml | cut -f 2 -d : | tr -d ' ')
ifeq ($(CI_COMMIT_SHORT_SHA),)
	CI_COMMIT_SHORT_SHA:=$(shell git rev-parse --short HEAD)
endif
COMPILE_DATE:=$(shell LC_TIME=C date +"%F %T %z")
SHORT_DATE:=$(shell LC_TIME=C date +"%Y%m%d")

PKG_NAME = wm-helper
DEB_VERSION = $(VERSION)-$(SHORT_DATE)
DEB_PATH = debian/$(PKG_NAME)
DEB_PKG = debian/$(PKG_NAME).deb
DEB_PKG_FINAL = debian/$(PKG_NAME)_$(DEB_VERSION)_all.deb

SRCS = $(wildcard src/*.cr) src/version_info.cr

bin/$(APP_NAME): lib $(SRCS)
	shards build

release: lib $(SRCS)
	shards build --release --link-flags=-s

src/version_info.cr: src/version_info.cr.in
	m4  -D M4_APP_VERSION="$(VERSION)" \
		-D M4_GIT_REVISION="$(CI_COMMIT_SHORT_SHA)" \
		-D M4_COMPILE_DATE="$(COMPILE_DATE)" \
		-P src/version_info.cr.in >src/version_info.cr

static: lib $(SRCS)
	podman build -t $(APP_NAME) .
	podman run --rm -it -v $(PWD):/src --name $(APP_NAME) localhost/$(APP_NAME) shards build --static

static-release: lib $(SRCS)
	podman build -t $(APP_NAME) .
	podman run --rm -it -v $(PWD):/src --name $(APP_NAME) localhost/$(APP_NAME) shards build --static --release --link-flags=-s

lib: shard.yml
	shards install

test:
	crystal spec

clean: deb-clean
	rm -f bin/$(APP_NAME)
	rm -f src/version_info.cr

deb: test static-release
	mkdir -p $(DEB_PATH)/usr/bin
	cp bin/$(APP_NAME) $(DEB_PATH)/usr/bin
	chmod +x $(DEB_PATH)/usr/bin/$(APP_NAME)
	mkdir -p $(DEB_PATH)/usr/share/doc/$(PKG_NAME)
	cp LICENSE $(DEB_PATH)/usr/share/doc/$(PKG_NAME)/copyright
	cp README.md $(DEB_PATH)/usr/share/doc/$(PKG_NAME)/
	mkdir -p $(DEB_PATH)/DEBIAN
	chmod 755 $(DEB_PATH)/DEBIAN
	sed <debian/control.tpl "s/{VERSION}/$(DEB_VERSION)/" >$(DEB_PATH)/DEBIAN/control
	dpkg-deb --root-owner-group --build $(DEB_PATH)
	mv $(DEB_PKG) $(DEB_PKG_FINAL)

deb-repo: deb
	update-deb-repo.sh $(DEB_PKG_FINAL)

deb-clean:
	rm -rf $(DEB_PATH)/usr
	rm -rf debian/*.deb
	rm -rf debian/*/DEBIAN

dist-clean: clean
	rm lib -rf
