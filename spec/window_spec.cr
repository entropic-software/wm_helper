require "./spec_helper"

include WMHelper

screen1 = LibXinerama::XineramaScreenInfo.new(
  screen_number: 0,
  x_org: 0,
  y_org: 0,
  width: 1024,
  height: 769,
)

screen2 = LibXinerama::XineramaScreenInfo.new(
  screen_number: 0,
  x_org: 1024,
  y_org: 0,
  width: 1024,
  height: 769,
)

dpy = LibX11::Display.new
screens = [screen1, screen2]

describe Window do
  it "has the correct size and position including frame" do
    SimulatedX11Connection.set_windows([
      SimulatedX11Window.new(10, Geometry.new(10, 10, 100, 100)),
    ])

    w1 = Window.new(10_u64, pointerof(dpy), 1_u64, screens, 0)

    w1.geometry.x.should eq(10)
    w1.geometry.y.should eq(10)
    w1.geometry.width.should eq(100)
    w1.geometry.height.should eq(100)
  end
end
