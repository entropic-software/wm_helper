require "./spec_helper"

include WMHelper

screen1 = LibXinerama::XineramaScreenInfo.new(
  screen_number: 0,
  x_org: 0,
  y_org: 0,
  width: 1024,
  height: 768,
)

screen2 = LibXinerama::XineramaScreenInfo.new(
  screen_number: 1,
  x_org: 1024,
  y_org: 0,
  width: 1024,
  height: 768,
)

dpy = LibX11::Display.new
screens = [screen1, screen2]

describe WM do
  ####################################
  # Screen info

  it "reports correct screen size for one screen" do
    screens = [
      LibXinerama::XineramaScreenInfo.new(
        screen_number: 0, x_org: 0, y_org: 0, width: 1024, height: 768,
      ),
    ]

    dsize = WM.display_size(screens)
    dsize.width.should eq(1024)
    dsize.height.should eq(768)
  end

  it "reports correct screen size for one screen, offset" do
    screens = [
      LibXinerama::XineramaScreenInfo.new(
        screen_number: 0, x_org: 10, y_org: 10, width: 1024, height: 768,
      ),
    ]

    dsize = WM.display_size(screens)
    dsize.width.should eq(1034)
    dsize.height.should eq(778)
  end

  it "reports correct screen size for two screens beside eachother" do
    screens = [
      LibXinerama::XineramaScreenInfo.new(
        screen_number: 0, x_org: 0, y_org: 0, width: 1024, height: 768,
      ),
      LibXinerama::XineramaScreenInfo.new(
        screen_number: 1, x_org: 1024, y_org: 0, width: 1024, height: 768,
      ),
    ]

    dsize = WM.display_size(screens)
    dsize.width.should eq(2048)
    dsize.height.should eq(768)
  end

  it "reports correct screen size for two screens offset" do
    screens = [
      LibXinerama::XineramaScreenInfo.new(
        screen_number: 0, x_org: 0, y_org: 0, width: 800, height: 600,
      ),
      LibXinerama::XineramaScreenInfo.new(
        screen_number: 1, x_org: 1000, y_org: 1000, width: 800, height: 600,
      ),
    ]

    dsize = WM.display_size(screens)
    dsize.width.should eq(1800)
    dsize.height.should eq(1600)
  end

  it "reports correct screen size for two screens on top of eachother" do
    screens = [
      LibXinerama::XineramaScreenInfo.new(
        screen_number: 0, x_org: 0, y_org: 0, width: 1024, height: 768,
      ),
      LibXinerama::XineramaScreenInfo.new(
        screen_number: 1, x_org: 0, y_org: 768, width: 1024, height: 768,
      ),
    ]

    dsize = WM.display_size(screens)
    dsize.width.should eq(1024)
    dsize.height.should eq(1536)
  end

  it "reports correct screen size for four screens in a square" do
    screens = [
      LibXinerama::XineramaScreenInfo.new(
        screen_number: 0, x_org: 0, y_org: 0, width: 1024, height: 768,
      ),
      LibXinerama::XineramaScreenInfo.new(
        screen_number: 1, x_org: 0, y_org: 768, width: 1024, height: 768,
      ),
      LibXinerama::XineramaScreenInfo.new(
        screen_number: 2, x_org: 1024, y_org: 0, width: 1024, height: 768,
      ),
      LibXinerama::XineramaScreenInfo.new(
        screen_number: 3, x_org: 1024, y_org: 768, width: 1024, height: 768,
      ),
    ]

    dsize = WM.display_size(screens)
    dsize.width.should eq(2048)
    dsize.height.should eq(1536)
  end

  ####################################
  # Move towards window

  it "moves window right toward other window" do
    SimulatedX11Connection.set_windows([
      SimulatedX11Window.new(10, Geometry.new(0, 0, 100, 100)),
      SimulatedX11Window.new(20, Geometry.new(300, 0, 100, 100)),
    ])

    w1 = Window.new(10_u64, pointerof(dpy), 1_u64, screens, 0)
    w2 = Window.new(20_u64, pointerof(dpy), 1_u64, screens, 0)

    WM.pack_window(w1, [w2], Direction::Right)

    w1.left.should eq(200)
    w1.top.should eq(0)
    w1.width.should eq(100)
    w1.height.should eq(100)
  end

  it "moves window left toward other window" do
    SimulatedX11Connection.set_windows([
      SimulatedX11Window.new(10, Geometry.new(300, 0, 100, 100)),
      SimulatedX11Window.new(20, Geometry.new(0, 0, 100, 100)),
    ])

    w1 = Window.new(10_u64, pointerof(dpy), 1_u64, screens, 0)
    w2 = Window.new(20_u64, pointerof(dpy), 1_u64, screens, 0)

    WM.pack_window(w1, [w2], Direction::Left)

    w1.geometry.x.should eq(100)
    w1.geometry.y.should eq(0)
    w1.geometry.width.should eq(100)
    w1.geometry.height.should eq(100)
  end

  it "moves window down toward other window" do
    SimulatedX11Connection.set_windows([
      SimulatedX11Window.new(10, Geometry.new(0, 0, 100, 100)),
      SimulatedX11Window.new(20, Geometry.new(0, 300, 100, 100)),
    ])

    w1 = Window.new(10_u64, pointerof(dpy), 1_u64, screens, 0)
    w2 = Window.new(20_u64, pointerof(dpy), 1_u64, screens, 0)

    WM.pack_window(w1, [w2], Direction::Down)

    w1.geometry.x.should eq(0)
    w1.geometry.y.should eq(200)
    w1.geometry.width.should eq(100)
    w1.geometry.height.should eq(100)
  end

  it "moves window up toward other window" do
    SimulatedX11Connection.set_windows([
      SimulatedX11Window.new(10, Geometry.new(0, 300, 100, 100)),
      SimulatedX11Window.new(20, Geometry.new(0, 0, 100, 100)),
    ])

    w1 = Window.new(10_u64, pointerof(dpy), 1_u64, screens, 0)
    w2 = Window.new(20_u64, pointerof(dpy), 1_u64, screens, 0)

    WM.pack_window(w1, [w2], Direction::Up)

    w1.geometry.x.should eq(0)
    w1.geometry.y.should eq(100)
    w1.geometry.width.should eq(100)
    w1.geometry.height.should eq(100)
  end

  ####################################
  # Move towards screen

  it "moves window right toward screen edge" do
    SimulatedX11Connection.set_windows([
      SimulatedX11Window.new(10, Geometry.new(300, 300, 100, 100)),
      SimulatedX11Window.new(20, Geometry.new(600, 600, 100, 100)),
    ])

    w1 = Window.new(10_u64, pointerof(dpy), 1_u64, screens, 0)
    w2 = Window.new(20_u64, pointerof(dpy), 1_u64, screens, 0)

    WM.pack_window(w1, [w2], Direction::Right)

    w1.geometry.x.should eq(screen1.width - 100)
    w1.geometry.y.should eq(300)
    w1.geometry.width.should eq(100)
    w1.geometry.height.should eq(100)
  end

  it "moves window left toward screen edge" do
    SimulatedX11Connection.set_windows([
      SimulatedX11Window.new(10, Geometry.new(300, 300, 100, 100)),
      SimulatedX11Window.new(20, Geometry.new(600, 600, 100, 100)),
    ])

    w1 = Window.new(10_u64, pointerof(dpy), 1_u64, screens, 0)
    w2 = Window.new(20_u64, pointerof(dpy), 1_u64, screens, 0)

    WM.pack_window(w1, [w2], Direction::Left)

    w1.geometry.x.should eq(0)
    w1.geometry.y.should eq(300)
    w1.geometry.width.should eq(100)
    w1.geometry.height.should eq(100)
  end

  it "moves window down toward screen edge" do
    SimulatedX11Connection.set_windows([
      SimulatedX11Window.new(10, Geometry.new(300, 300, 100, 100)),
      SimulatedX11Window.new(20, Geometry.new(600, 600, 100, 100)),
    ])

    w1 = Window.new(10_u64, pointerof(dpy), 1_u64, screens, 0)
    w2 = Window.new(20_u64, pointerof(dpy), 1_u64, screens, 0)

    WM.pack_window(w1, [w2], Direction::Down)

    w1.geometry.x.should eq(300)
    w1.geometry.y.should eq(screen1.height - 100)
    w1.geometry.width.should eq(100)
    w1.geometry.height.should eq(100)
  end

  it "moves window up toward screen edge" do
    SimulatedX11Connection.set_windows([
      SimulatedX11Window.new(10, Geometry.new(300, 300, 100, 100)),
      SimulatedX11Window.new(20, Geometry.new(600, 600, 100, 100)),
    ])

    w1 = Window.new(10_u64, pointerof(dpy), 1_u64, screens, 0)
    w2 = Window.new(20_u64, pointerof(dpy), 1_u64, screens, 0)

    WM.pack_window(w1, [w2], Direction::Up)

    w1.geometry.x.should eq(300)
    w1.geometry.y.should eq(0)
    w1.geometry.width.should eq(100)
    w1.geometry.height.should eq(100)
  end
end
