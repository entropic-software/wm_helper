require "spec"
require "../src/window"
require "../src/wm"

# Simulated windows
class SimulatedX11Window
  property id : LibX11::Window
  property x = 10
  property y = 20
  property width = 100
  property height = 200
  property hints
  property attributes
  property stack_mode = LibX11::StackMode::Above
  property prop_wm_state = [] of UInt64
  property prop_wm_name
  property prop_net_wm_state = [] of UInt64
  property prop_net_wm_desktop = [] of UInt64
  property prop_net_frame_extents = [] of UInt64

  def initialize(@id, geometry)
    @prop_net_frame_extents = [5_u64, 5_u64, 23_u64, 5_u64] # left, right, top, bottom

    # The geometry parameter is the outer geometry, including frame
    @x = geometry.x + @prop_net_frame_extents[0]
    @y = geometry.y + @prop_net_frame_extents[2]
    @width = geometry.width - @prop_net_frame_extents[0] - @prop_net_frame_extents[1]
    @height = geometry.height - @prop_net_frame_extents[2] - @prop_net_frame_extents[3]

    @hints = LibX11::SizeHints.new
    @hints.min_width = 10
    @hints.min_height = 20
    @hints.max_width = 1000
    @hints.max_height = 2000
    @hints.win_gravity = LibX11::StaticGravity

    @attributes = LibX11::XWindowAttributes.new
    @attributes.border_width = 0

    @prop_wm_name = "unnamed, id: #{@id}"
  end
end

class SimulatedX11Connection
  @@windows : Array(SimulatedX11Window) = [] of SimulatedX11Window

  def self.windows
    @@windows
  end

  def self.set_windows(windows)
    @@windows = windows
  end
end

# Stub for X11 communication
module WMHelper::X11
  def self.get_input_focus(dpy : LibX11::Display*) : LibX11::Window
    SimulatedX11Connection.windows[0].id
  end

  def self.warp_pointer(
    dpy : LibX11::Display*,
    root : LibX11::RootWindow,
    x : Int32,
    y : Int32
  )
  end

  def self.get_wm_normal_hints(
    dpy : LibX11::Display*,
    win_id : LibX11::Window
  ) : LibX11::SizeHints
    win = SimulatedX11Connection.windows.find { |w| w.id == win_id }
    raise "Invalid window: #{win_id}" if win.nil?
    win.hints
  end

  def self.get_window_attributes(
    dpy : LibX11::Display*,
    win_id : LibX11::Window
  ) : LibX11::XWindowAttributes
    win = SimulatedX11Connection.windows.find { |w| w.id == win_id }
    raise "Invalid window: #{win_id}" if win.nil?
    win.attributes
  end

  def self.get_size(
    dpy : LibX11::Display*,
    win_id : LibX11::Window
  ) : Size
    win = SimulatedX11Connection.windows.find { |w| w.id == win_id }
    raise "Invalid window: #{win_id}" if win.nil?
    Size.new(width: win.width, height: win.height)
  end

  def self.get_position(
    dpy : LibX11::Display*,
    win_id : LibX11::Window,
    root : LibX11::RootWindow
  ) : Position
    win = SimulatedX11Connection.windows.find { |w| w.id == win_id }
    raise "Invalid window: #{win_id}" if win.nil?
    Position.new(x: win.x, y: win.y)
  end

  def self.configure_window(
    dpy : LibX11::Display*,
    win_id : LibX11::Window,
    value_mask,
    values
  )
    win = SimulatedX11Connection.windows.find { |w| w.id == win_id }
    raise "Invalid window: #{win_id}" if win.nil?

    win.x = values.x if (value_mask & (LibX11::CWX)) != 0
    win.y = values.y if (value_mask & (LibX11::CWY)) != 0
    win.width = values.width if (value_mask & (LibX11::CWWidth)) != 0
    win.height = values.height if (value_mask & (LibX11::CWHeight)) != 0
    win.stack_mode = values.stack_mode if (value_mask & (LibX11::CWStackMode)) != 0
  end
end

macro m_get_win(win_id)
  win = SimulatedX11Connection.windows.find { |w| w.id == {{win_id}} }
  raise "Invalid window: {{ win_id }}" if win.nil?
end

# Stub for properties
module WMHelper::Properties
  def self.get_atom_name(dpy : LibX11::Display*, type_id) : String
    X11.get_atom_name(dpy, type_id)
  end

  def self.get_wm_state(dpy, win_id) : Array(UInt64)
    win = SimulatedX11Connection.windows.find { |w| w.id == win_id }
    raise "Invalid window: #{win_id}" if win.nil?
    win.prop_wm_state
  end

  def self.get_net_wm_state(dpy, win_id) : Array(UInt64)
    win = SimulatedX11Connection.windows.find { |w| w.id == win_id }
    raise "Invalid window: #{win_id}" if win.nil?
    win.prop_net_wm_state
  end

  def self.get_wm_name(dpy, win_id) : String
    win = SimulatedX11Connection.windows.find { |w| w.id == win_id }
    raise "Invalid window: #{win_id}" if win.nil?
    win.prop_wm_name
  end

  def self.get_net_wm_desktop(dpy, win_id) : Array(UInt64)
    win = SimulatedX11Connection.windows.find { |w| w.id == win_id }
    raise "Invalid window: #{win_id}" if win.nil?
    win.prop_net_wm_desktop
  end

  def self.get_net_frame_extents(dpy, win_id : LibX11::Window) : Array(UInt64)
    win = SimulatedX11Connection.windows.find { |w| w.id == win_id }
    raise "Invalid window: #{win_id}" if win.nil?
    win.prop_net_frame_extents
  end

  def self.get_net_active_window(dpy, root : LibX11::RootWindow) : Array(UInt64)
    [SimulatedX11Connection.windows.map { |w| w.id }.first]
  end

  def self.get_net_client_list_stacking(dpy, root : LibX11::RootWindow) : Array(UInt64)
    SimulatedX11Connection.windows.map { |w| w.id }
  end
end
