Package: wm-helper
Version: {VERSION}
Section: x11
Priority: optional
Architecture: amd64
Maintainer: Tomas Åkesson <tomas@entropic.se>
Homepage: https://gitlab.com/entropic-software/wm_helper
Description: Add-on functionality for X11 window-managers
