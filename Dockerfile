FROM docker.io/crystallang/crystal:latest-alpine
RUN apk add --no-cache libx11-static libxcb-static libxinerama-dev
WORKDIR /src
