# About wm_helper

Add-on functionality for X11 window-managers.

This application lets you move around, resize and focus windows based on
surrounding windows. It's basically an addition to your regular window manager.
The idea is to bind keyboard commands to wm_helper which will manipulate
the currently active window.

There is no connection to the window manager, so wm_helper can be used with
any wm (or at least wm's that behave, see the Bugs section for issues).

It's mainly aimed at stacking window managers that allows the user to organize
windows manually. It's not really usable for tiling window managers.

Many of wm_helper's functions are inspired by functionality built-in to Fvwm
and Sawfish. It was created mainly to get these functions when using Xfwm4
or other window managers.

Actions currently built in:

- Move window in a given direction until it hits another window or the screen
  boundary. This is called "shuffle" in Fvwm3 and "pack" in Sawfish.
- Extend a window in a given direction until it hits another window or the
  screen boundary. This is called "grow" in Fvwm and Sawfish.
- Extend a window in all directions until it's borders hits another window or
  the screen boundary.
- Focus a neighbouring window in a given direction. In Fvwm this can be done
  with a combination of the `Direction`, `Focus` and `WarpToWindow` functions.
- Raise and lower the active window.

## Status

Alpha quality; under heavy development.
There are problems interacting with some window managers.
Interaction with Fvwm2 and Xfwm4 is tested and works.

## Building

You can build either with static or dynamic linking.

Static will include all needed libraries in the finished executable, and it
will therefore be larger. But it will work on other systems than which it is
built on, without having to install any extra libraries.

Dynamic needs the required libraries (and correct versions of them) to be
installed on any computers the application should be run on.

### Static linking

Dependencies for building:

- Podman
- m4

(Docker should probably also work with minor changes to the Makefile, but it's
untested.)

Run `make static` or `make static-release` to build the binary.

### Dynamic linking

Dependencies for building:

- [Crystal](https://crystal-lang.org)
- libxinerama-dev
- libx11-dev
- m4

Run `make` or `make release` to build the binary.

## Installation

Copy `bin/wm_helper` to a directory in your path, like `$HOME/.local/bin`
(for the current user) or `/usr/local/bin` (for system-wide use).

## Usage

Run `wm_helper -h` for a list of actions. See About for description about
the actions.

Use your window manager / desktop environment configuration to bind a keyboard
shortcut to a command.
For example these are my bindings ("Super" is a.k.a. "Win"):

- Super+PageUp: `wm_helper --raise`
- Super+PageUp: `wm_helper --lower`
- Super+Right: `wm_helper --fr`
- Ctrl+Super+Right: `wm_helper --gr`
- Shift+Super+Right: `wm_helper --mr`
- Ctrl+Super+Return: `wm_helper --ga`

(Repeat for left, up and down)

## Bugs

- Grow and move doesn't work over multiple screens
- If a window has size steps (SizeHints.width_inc, SizeHints.height_inc),
  like f.ex. xterm, consider these (in addition to base_width and base_height)
  when calculating new size/pos
- If wm does not support `_NET_CLIENT_LIST_STACKING` no other windows are found
- Under Blackbox, Fluxbox etc, windows are moved to the wrong position

## Contributors

Created by Tomas Åkesson <tomas@entropic.se>

See LICENSE for license details.

Project homepage: https://gitlab.com/entropic-software/wm-helper
