require "./x11_properties.cr"

module WMHelper::WM
  def self.display_size(screens : Array(LibXinerama::XineramaScreenInfo)) : Size
    # Get the screen with the largest x_org/y_org and use it for determining
    # the total screen area
    x_max = screens.sort { |a, b| a.x_org <=> b.x_org }.last
    y_max = screens.sort { |a, b| a.y_org <=> b.y_org }.last

    Size.new(
      width: x_max.width + x_max.x_org,
      height: y_max.height + y_max.y_org,
    )
  end

  def self.get_current_desktop(dpy : LibX11::Display*, root : LibX11::RootWindow) : Int64
    prop_values = Properties.get_net_wm_current_desktop(dpy, root)
    return DesktopNone if prop_values.size != 1

    val : UInt64 = prop_values[0]
    if val == UInt64::MAX
      # UInt64::MAX represents -1 in Int64, which means "sticky"
      DesktopSticky
    else
      val.to_i64
    end
  end

  # If this window's left border is to the right of the other window's right
  # border. In other words: the window's can not overlap.
  # In addition, they must not be adjacent.
  def self.can_extend_to?(
    direction : Direction,
    this : Window,
    other : Window
  ) : Bool
    case direction
    in Direction::Left
      this.right < other.left && is_in_horizontal_path?(this, other)
    in Direction::Right
      this.left > other.right && is_in_horizontal_path?(this, other)
    in Direction::Up
      this.bottom < other.top && is_in_vertical_path?(this, other)
    in Direction::Down
      this.top > other.bottom && is_in_vertical_path?(this, other)
    in Direction::None
      false
    end
  end

  def self.can_switch_focus_to?(
    direction : Direction,
    this_center : Position,
    other_center : Position
  ) : Bool
    case direction
    in Direction::Left
      this_center.x < other_center.x
    in Direction::Right
      this_center.x > other_center.x
    in Direction::Up
      this_center.y < other_center.y
    in Direction::Down
      this_center.y > other_center.y
    in Direction::None
      false
    end
  end

  def self.is_in_horizontal_path?(this : Window, other : Window) : Bool
    !(this.top >= other.bottom || this.bottom <= other.top)
  end

  def self.is_in_vertical_path?(this : Window, other : Window) : Bool
    !(this.left >= other.right || this.right <= other.left)
  end

  # Euclidian distance
  # Take into account that the more angle between windows, the lower sort
  # points they get.
  # Also take into account the higher up in the stack, the better sort
  # points (lower distance) they get.
  def self.euclidean_distance_to(
    direction : Direction,
    this_center : Position,
    other : Window
  ) : Int32
    d = Math.sqrt(
      (other.get_center.x - this_center.x)**2 +
      (other.get_center.y - this_center.y)**2
    )

    case direction
    in Direction::Left
      angle = Math.atan2(
        this_center.y - other.get_center.y, this_center.x - other.get_center.x
      ) * 180 / Math::PI
    in Direction::Right
      angle = Math.atan2(
        other.get_center.y - this_center.y, other.get_center.x - this_center.x
      ) * 180 / Math::PI
    in Direction::Up
      angle = Math.atan2(
        this_center.x - other.get_center.x, this_center.y - other.get_center.y
      ) * 180 / Math::PI
    in Direction::Down
      angle = Math.atan2(
        other.get_center.x - this_center.x, other.get_center.y - this_center.y
      ) * 180 / Math::PI
    in Direction::None
      angle = 0
    end

    if angle > 180
      angle = (180 - angle).abs
    end
    angle = angle.abs

    devel_puts "eucl dist: #{d.to_i}, angle: #{angle.to_i}, other stacking: #{other.stacking}: " +
               other.get_title
    return d.to_i32 + other.stacking + angle.to_i * 10
  end

  # Straight distance
  def self.distance_to(
    direction : Direction,
    this : Window,
    other : Window
  ) : Int32
    case direction
    in Direction::Left
      this.left - other.right
    in Direction::Right
      other.left - this.right
    in Direction::Up
      this.top - other.bottom
    in Direction::Down
      other.top - this.bottom
    in Direction::None
      Int32::MAX
    end
  end

  def self.distance_to_screen_edge(
    direction : Direction,
    window : Window,
    screen : LibXinerama::XineramaScreenInfo
  ) : Int32
    case direction
    in Direction::Left
      window.left - screen.x_org.to_i32
    in Direction::Right
      screen.x_org.to_i32 + screen.width.to_i32 - window.right
    in Direction::Up
      window.top - screen.y_org.to_i32
    in Direction::Down
      screen.y_org.to_i32 + screen.height.to_i32 - window.bottom
    in Direction::None
      Int32::MAX
    end
  end

  # TODO: When no window manager is running, window_id will be
  # LibX11::PointerRoot, so if this happens, try to find the active window
  # in some other way if possible.
  def self.get_active_window(dpy : LibX11::Display*, root : LibX11::RootWindow) : LibX11::Window
    window_ids = Properties.get_net_active_window(dpy, root)

    if window_ids.size > 1 && window_ids[0] != 0
      window_ids[0]
    else
      X11.get_input_focus(dpy)
    end
  end

  # Get a list of windows on the current desktop
  def self.windows_on_desktop(
    dpy : LibX11::Display*,
    root : LibX11::RootWindow,
    desktop : Int64,
    screens : Array(LibXinerama::XineramaScreenInfo)
  ) : Array(Window)
    # TODO: If wm does not support "_NET_CLIENT_LIST_STACKING"
    # go thru the tree instead. (low prio)
    list = Properties.get_net_client_list_stacking(dpy, root)

    list.reverse.each_with_index.map { |win_id, idx|
      Window.new(win_id, dpy, root, screens, idx)
    }.to_a.select { |win|
      win.get_desktop == desktop ||
        win.get_desktop == DesktopSticky ||
        desktop == DesktopSticky
    }
  end

  def self.mid_window_x(geo : Geometry)
    geo.x + (geo.width // 2)
  end

  def self.mid_window_y(geo : Geometry)
    geo.y + (geo.height // 2)
  end

  def self.warp_pointer(dpy : LibX11::Display*, root : LibX11::RootWindow, x : Int32, y : Int32)
    X11.warp_pointer(dpy, root, x, y)
  end

  def self.warp_pointer_to_titlebar(dpy : LibX11::Display*, root : LibX11::RootWindow, geo : Geometry)
    self.warp_pointer(
      dpy,
      root,
      geo.x + (geo.width // 2),
      geo.y + 6
    )
  end

  def self.focus_window(
    current_window : Window,
    other_windows : Array(Window),
    direction : Direction
  ) : Window
    return current_window if direction == Direction::None

    # TODO: Don't select windows that have any of _NET_WM_STATE:
    # _NET_WM_STATE_BELOW, _NET_WM_STATE_SKIP_PAGER, _NET_WM_STATE_SKIP_TASKBAR
    target_windows = other_windows.select { |w|
      WM.can_switch_focus_to?(direction, w.get_center, current_window.get_center)
    }.reject { |w|
      w.ignore_focus?
    }.sort { |a, b|
      WM.euclidean_distance_to(direction, current_window.get_center, a) <=>
        WM.euclidean_distance_to(direction, current_window.get_center, b)
    }

    if target_windows.empty?
      devel_puts "no match, select current"
      return current_window
    else
      target_window = target_windows.first
      devel_puts "target: " + target_window.get_title
      target_window.take_focus
      return target_window
    end
  end

  def self.focus_window_2(
    mouse_pos : Position,
    other_windows : Array(Window),
    direction : Direction
  ) : Window?
    return nil if direction == Direction::None

    # TODO: Don't select windows that have any of _NET_WM_STATE:
    # _NET_WM_STATE_BELOW, _NET_WM_STATE_SKIP_PAGER, _NET_WM_STATE_SKIP_TASKBAR
    target_windows = other_windows.select { |w|
      WM.can_switch_focus_to?(direction, w.get_center, mouse_pos)
    }.reject { |w|
      w.ignore_focus?
    }.sort { |a, b|
      WM.euclidean_distance_to(direction, mouse_pos, a) <=>
        WM.euclidean_distance_to(direction, mouse_pos, b)
    }

    if target_windows.empty?
      devel_puts "no match, don't do anything"
      return nil
    else
      target_window = target_windows.first
      devel_puts "target: " + target_window.get_title
      target_window.take_focus
      return target_window
    end
  end

  def self.pack_window(
    current_window : Window,
    other_windows : Array(Window),
    direction : Direction
  )
    return if direction == Direction::None

    target_windows = other_windows.select { |w|
      WM.can_extend_to?(direction, w, current_window)
    }.sort { |a, b|
      WM.distance_to(direction, current_window, a) <=> WM.distance_to(direction, current_window, b)
    }

    screen_distance = WM.distance_to_screen_edge(direction, current_window, current_window.screen)
    devel_puts "SCREEN dist: #{screen_distance}"

    devel_puts "pack: direction: #{direction}, windows and distances:"
    target_windows.each do |w|
      devel_puts " - title: '#{w.get_title}' " + WM.distance_to(direction, current_window, w).to_s
    end

    if target_windows.empty?
      devel_puts "no match, move to screen border"
      current_window.move_to_screen_edge(direction)
    else
      target_window = target_windows.first
      if screen_distance < WM.distance_to(direction, current_window, target_window
         )
        devel_puts "pack: screen border nearer than " + target_window.get_title
        current_window.move_to_screen_edge(direction)
      else
        devel_puts "pack: target: " + target_window.get_title
        current_window.pack_to(direction, target_window)
      end
    end
  end

  # TODO: If already adjacent a window, should we grow over it, or not grow?
  # TODO: Do the resize in steps: First resize up, check limits, resize down,
  # check limits, etc. Note that "resize" does not mean "physically resize",
  # but actually simulate the resize. This is so the window grows first
  # vertically then horizontally, so as to not cover too much space.
  def self.grow_window(
    current_window : Window,
    other_windows : Array(Window),
    directions : Array(Direction),
    screen_size : Size
  )
    limit_left = current_window.left
    limit_right = current_window.right
    limit_top = current_window.top
    limit_bottom = current_window.bottom

    directions.each do |direction|
      target_windows = other_windows.select { |w|
        WM.can_extend_to?(direction, w, current_window)
      }.sort { |a, b|
        WM.distance_to(direction, current_window, a) <=> WM.distance_to(direction, current_window, b)
      }

      devel_puts "grow (#{direction}): distance:"
      target_windows.each do |w|
        devel_puts "title: #{w.get_title} " + WM.distance_to(direction, current_window, w).to_s
      end

      if target_windows.empty?
        devel_puts "grow (#{direction}): no match, grow to screen border"

        case direction
        in Direction::Left
          limit_left = 0
        in Direction::Right
          limit_right = screen_size.width
        in Direction::Up
          limit_top = 0
        in Direction::Down
          limit_bottom = screen_size.height
        in Direction::None
        end
      else
        target_window = target_windows.first
        devel_puts "grow (#{direction}): target: " + target_window.get_title

        case direction
        in Direction::Left
          limit_left = target_window.right
        in Direction::Right
          limit_right = target_window.left
        in Direction::Up
          limit_top = target_window.bottom
        in Direction::Down
          limit_bottom = target_window.top
        in Direction::None
        end
      end
    end

    new_geo = Geometry.new(
      x: limit_left,
      y: limit_top,
      width: limit_right - limit_left,
      height: limit_bottom - limit_top,
    )

    current_window.move_and_or_resize(new_geo)
  end
end
