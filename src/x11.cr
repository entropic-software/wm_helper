module WMHelper::X11
  def self.get_input_focus(dpy : LibX11::Display*) : LibX11::Window
    status = LibX11.get_input_focus(dpy, out window_id, out dummy_revert)
    raise "Failed to get active window" if !status
    raise "Failed to get active window" if window_id == LibX11::None
    raise "Failed to get active window" if window_id == LibX11::PointerRoot
    window_id
  end

  def self.warp_pointer(
    dpy : LibX11::Display*,
    root : LibX11::RootWindow,
    x : Int32,
    y : Int32
  )
    status = LibX11.warp_pointer(
      dpy, LibX11::None, root,
      LibX11::None, LibX11::None, LibX11::None, LibX11::None,
      x, y
    )
    STDERR.puts "Failed to warp pointer to #{x}x#{y}" if status == 0
  end

  def self.get_pointer_pos(dpy : LibX11::Display*, root : LibX11::RootWindow) : Position
    status = LibX11.query_pointer(
      dpy,
      root,
      out root_return,
      out child_return,
      out root_x_return,
      out root_y_return,
      out win_x_return,
      out win_y_return,
      out mask_return
    )
    STDERR.puts "Failed to get pointer position" if status == 0
    Position.new(x: root_x_return, y: root_y_return)
  end

  def self.get_wm_normal_hints(
    dpy : LibX11::Display*,
    win_id : LibX11::Window
  ) : LibX11::SizeHints
    status = LibX11.get_wm_normal_hints(dpy, win_id, out size_hints, out supplied_hints)
    raise X11Error.new "Failed to get normal hints of window #{win_id}" if status == 0
    size_hints
  end

  def self.get_window_attributes(
    dpy : LibX11::Display*,
    win_id : LibX11::Window
  ) : LibX11::XWindowAttributes
    status = LibX11.get_window_attributes(dpy, win_id, out attrs)
    raise X11Error.new "Failed to get window attributes of #{win_id}" if status == 0
    attrs
  end

  def self.get_size(
    dpy : LibX11::Display*,
    win_id : LibX11::Window
  ) : Size
    status = LibX11.get_geometry(
      dpy, win_id, out dummy_root, out dummy_x, out dummy_y,
      out width, out height, out dummy_border_width, out dummy_depth
    )
    raise X11Error.new "Failed to get window size of window #{win_id}" if status == 0
    Size.new(width: width.to_i32, height: height.to_i32)
  end

  def self.get_position(
    dpy : LibX11::Display*,
    win_id : LibX11::Window,
    root : LibX11::RootWindow
  ) : Position
    status = LibX11.translate_coordinates(
      dpy, win_id, root, 0, 0, out x, out y, out dummy_child
    )
    raise X11Error.new "Failed to get window position of window #{win_id}" if status == 0
    Position.new(x: x.to_i32, y: y.to_i32)
  end

  def self.configure_window(
    dpy : LibX11::Display*,
    win_id : LibX11::Window,
    value_mask,
    values
  )
    status = LibX11.configure_window(dpy, win_id, value_mask, pointerof(values))
    raise X11Error.new "Failed to configure window #{win_id}" if status == 0
  end
end
