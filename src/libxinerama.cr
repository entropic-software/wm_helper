@[Link("xinerama")]
lib LibXinerama
  struct XineramaScreenInfo
    screen_number : LibC::Int
    x_org : LibC::Short
    y_org : LibC::Short
    width : LibC::Short
    height : LibC::Short
  end

  fun xinerama_query_screens = XineramaQueryScreens(
    dpy : Pointer(LibX11::Display),
    number : Pointer(LibC::Int)
  ) : Pointer(XineramaScreenInfo)

  fun xinerama_is_active = XineramaIsActive(
    dpy : Pointer(LibX11::Display)
  ) : Bool
end
