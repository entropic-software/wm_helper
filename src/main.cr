# Definitions
#
# "Display" = The entire display, consisting of all the monitors
#  +--------------------------------+
#  |                                |
#  |             display            |
#  |                                |
#  +--------------------------------+
#
# "Screen" = A part of the display as visible on a physical monitor
#  +--------------------------------+
#  |          |          |          |
#  | screen 1 | screen 2 | screen 3 |
#  |          |          |          |
#  +--------------------------------+

require "option_parser"
require "./libx11.cr"
require "./libxinerama.cr"
require "./window.cr"
require "./wm.cr"
require "./wm_helper.cr"
require "./version_info.cr"

module WMHelper
  begin
    dpy : Pointer(LibX11::Display) = LibX11.open_display(ENV["DISPLAY"])
  rescue ke : KeyError
    die "DISPLAY not set"
  end

  die "Cannot open display" if !dpy
  die "Xinerama not enabled" if !LibXinerama.xinerama_is_active(dpy)

  # Get screens
  num_screens : LibC::Int = 0
  xs = LibXinerama.xinerama_query_screens(dpy, pointerof(num_screens))
  screens = [] of LibXinerama::XineramaScreenInfo
  (0..num_screens - 1).each do |i|
    screens << xs[i]
  end
  LibX11.free(xs)

  root : LibX11::RootWindow = LibX11.default_root_window(dpy)

  window_id : LibX11::Window = 0_u64
  action = Action::None
  raise_after_action = true
  direction : Direction = Direction::None
  directions = [] of Direction

  OptionParser.parse do |parser|
    parser.banner = "Usage: wm_helper [arguments]"

    parser.on(
      "-w WINDOW_ID",
      "Select a window instead of the currectly active. " +
      "May be decimal or hexadecimal."
    ) do |o|
      begin
        if o.starts_with?("0x")
          window_id = o.to_u64(prefix: true)
        else
          window_id = o.to_u64
        end
      rescue e : ArgumentError
        die "#{o} is not a valid window-id"
      end
    end

    parser.on("--lower", "Send window to the bottom of the stack") do
      action = Action::Lower
    end
    parser.on("--raise", "Send window to the top of the stack") do
      action = Action::Raise
    end

    parser.on("--fu", "Focuses the next window upwards") do
      action = Action::Focus
      direction = Direction::Up
    end
    parser.on("--fd", "Focuses the next window downwards") do
      action = Action::Focus
      direction = Direction::Down
    end
    parser.on("--fl", "Focuses the next window left") do
      action = Action::Focus
      direction = Direction::Left
    end
    parser.on("--fr", "Focuses the next window right") do
      action = Action::Focus
      direction = Direction::Right
    end

    parser.on("--mu", "Moves the window up until it reaches another window, or screen edge") do
      action = Action::Pack
      direction = Direction::Up
    end
    parser.on("--md", "Moves the window down until it reaches another window, or screen edge") do
      action = Action::Pack
      direction = Direction::Down
    end
    parser.on("--ml", "Moves the window left until it reaches another window, or screen edge") do
      action = Action::Pack
      direction = Direction::Left
    end
    parser.on("--mr", "Moves the window right until it reaches another window, or screen edge") do
      action = Action::Pack
      direction = Direction::Right
    end

    parser.on("--gu", "Grows the window up until it reaches another window, or screen edge") do
      action = Action::Grow
      directions = [Direction::Up]
    end
    parser.on("--gd", "Grows the window down until it reaches another window, or screen edge") do
      action = Action::Grow
      directions = [Direction::Down]
    end
    parser.on("--gl", "Grows the window left until it reaches another window, or screen edge") do
      action = Action::Grow
      directions = [Direction::Left]
    end
    parser.on("--gr", "Grows the window right until it reaches another window, or screen edge") do
      action = Action::Grow
      directions = [Direction::Right]
    end
    parser.on("--ga", "Grows the window in all directions until the borders reaches another window, or screen edge") do
      action = Action::Grow
      # Note: The order of these directions are fairly important.
      # See `grow_window` for more info.
      directions = [Direction::Up, Direction::Down, Direction::Left, Direction::Right]
    end
    parser.on("--gv", "Grows the window vertically until the borders reaches another window, or screen edge") do
      action = Action::Grow
      directions = [Direction::Up, Direction::Down]
    end
    parser.on("--gh", "Grows the window horizontally until the borders reaches another window, or screen edge") do
      action = Action::Grow
      directions = [Direction::Left, Direction::Right]
    end

    parser.on("-h", "--help", "Show this help") do
      STDERR.puts parser
      exit
    end

    parser.on("-V", "--version", "Show version information") do
      STDERR.puts "wm_helper version #{VERSION}, rev #{VERSION_GIT}, compiled at #{COMPILE_DATE}"
      STDERR.puts "Copyright (c) 2023 Tomas Åkesson"
      exit
    end

    parser.invalid_option do |flag|
      STDERR.puts "ERROR: #{flag} is not a valid option."
      STDERR.puts parser
      exit(1)
    end
  end

  if window_id == 0
    window_id = WM.get_active_window(dpy, root)
    devel_puts "detected window: #{window_id}"
  end

  current_desktop = WM.get_current_desktop(dpy, root)

  current_window : Window? = nil
  begin
    current_window = Window.new(window_id, dpy, root, screens, 0)
    devel_pp current_window.to_s
  rescue xe : X11Error
    # Only "focus"-functions will work on "no window" or "invalid window":
    # In that case, use current mouse position instead.
    if action != Action::Focus
      die STDERR.puts xe
    end
  end

  # Get a list of all visible windows, except the current, on the same desktop
  other_windows = WM.windows_on_desktop(dpy, root, current_desktop, screens).reject do |w|
    w.is_hidden?
  end
  if !current_window.nil?
    other_windows = other_windows.reject do |w|
      w.id == current_window.id
    end
  end

  devel_puts "Other Windows:"
  other_windows.each do |w|
    devel_pp w.to_s
  end

  if current_window.nil?
    mouse_pos = X11.get_pointer_pos(dpy, root)
    new_window = WM.focus_window_2(mouse_pos, other_windows, direction)
    WM.warp_pointer_to_titlebar(dpy, root, new_window.geometry) if !new_window.nil?
  else
    case action
    in Action::Focus
      new_window = WM.focus_window(current_window, other_windows, direction)

      WM.warp_pointer_to_titlebar(dpy, root, new_window.geometry)
    in Action::Pack
      WM.pack_window(current_window, other_windows, direction)

      WM.warp_pointer_to_titlebar(dpy, root, current_window.geometry)
    in Action::Grow
      WM.grow_window(
        current_window,
        other_windows,
        directions,
        WM.display_size(screens)
      )

      WM.warp_pointer_to_titlebar(dpy, root, current_window.geometry)
    in Action::Lower
      current_window.to_bottom
    in Action::Raise
      current_window.to_top
    in Action::None
      STDERR.puts "No action"
    end
  end

  LibX11.flush(dpy)
  LibX11.close_display(dpy)
end
