module WMHelper
  DesktopNone   = -2_i64
  DesktopSticky = -1_i64

  class X11Error < Exception
  end

  enum Action
    None
    Focus
    Pack
    Grow
    Raise
    Lower
    # TODO: Center window on screen
    # Center
  end

  enum Direction
    None
    Left
    Right
    Up
    Down
  end

  record Position,
    x : Int32,
    y : Int32 do
  end

  struct Size
    getter width : Int32
    getter height : Int32

    def initialize(@width, @height)
      if @width < 0 || @height < 0
        raise "Negative Size: #{self}"
      end
    end
  end

  struct Geometry
    getter x : Int32
    getter y : Int32
    getter width : Int32
    getter height : Int32

    def initialize(@x, @y, @width, @height)
      if @width < 0 || @height < 0
        raise "Negative Geometry: #{self}"
      end
    end
  end

  record FrameExtents,
    left : UInt64,
    right : UInt64,
    top : UInt64,
    bottom : UInt64 do
    def +(extra_border : Int32)
      FrameExtents.new(
        left: self.left + extra_border,
        right: self.right + extra_border,
        top: self.top + extra_border,
        bottom: self.bottom + extra_border
      )
    end
  end
end

def die(msg)
  STDERR.puts msg.to_s
  exit 1
end

def devel_puts(msg)
  STDERR.puts msg.to_s if ENV.has_key?("WM_HELPER_DEBUG")
end

def devel_pp(data)
  pp data if ENV.has_key?("WM_HELPER_DEBUG")
end

def between?(value : Number, low : Number, high : Number) : Bool
  value > low && value < high
end
