module WMHelper::Properties
  def self.get_atom_name(dpy : LibX11::Display*, type_id) : String
    X11.get_atom_name(dpy, type_id)
  end

  def self.get_wm_state(dpy, win_id) : Array(UInt64)
    X11.get_property(dpy, win_id, "WM_STATE")
  end

  def self.get_net_wm_state(dpy, win_id) : Array(UInt64)
    X11.get_property(dpy, win_id, "_NET_WM_STATE")
  end

  def self.get_wm_name(dpy, win_id) : String
    X11.get_string_property(dpy, win_id, "WM_NAME")
  end

  def self.get_net_wm_desktop(dpy, win_id) : Array(UInt64)
    X11.get_property(dpy, win_id, "_NET_WM_DESKTOP")
  end

  def self.get_net_wm_current_desktop(dpy, win_id) : Array(UInt64)
    X11.get_property(dpy, win_id, "_NET_CURRENT_DESKTOP")
  end

  def self.get_net_frame_extents(dpy, win_id) : Array(UInt64)
    X11.get_property(dpy, win_id, "_NET_FRAME_EXTENTS")
  end

  def self.get_net_active_window(dpy, root : LibX11::RootWindow) : Array(UInt64)
    X11.get_property(dpy, root, "_NET_ACTIVE_WINDOW")
  end

  def self.get_net_client_list_stacking(dpy, root : LibX11::RootWindow) : Array(UInt64)
    X11.get_property(dpy, root, "_NET_CLIENT_LIST_STACKING")
  end
end
