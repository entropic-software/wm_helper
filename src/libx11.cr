@[Link("x11")]
lib LibX11
  alias Atom = LibC::ULong
  alias Colormap = LibC::ULong
  alias Cursor = LibC::ULong
  alias Drawable = LibC::ULong
  alias GContext = LibC::ULong
  alias Pixmap = LibC::ULong
  alias Status = LibC::Int
  alias Time = LibC::ULong
  alias Window = LibC::ULong
  alias RootWindow = LibC::ULong
  alias XPointer = LibC::Char*
  alias WinGravity = LibC::Int

  None            = 0_i64 # universal null resource or null atom
  AnyPropertyType = 0_u64 # special Atom, passed to GetProperty
  PointerRoot     = 1_i64 # focus window in SetInputFocus
  CurrentTime     = 0_u64 # special Time

  # ConfigureWindow structure
  CWX           = (1_u32 << 0)
  CWY           = (1_u32 << 1)
  CWWidth       = (1_u32 << 2)
  CWHeight      = (1_u32 << 3)
  CWBorderWidth = (1_u32 << 4)
  CWSibling     = (1_u32 << 5)
  CWStackMode   = (1_u32 << 6)

  enum StackMode
    Above    = 0
    Below    = 1
    TopIf    = 2
    BottomIf = 3
    Opposite = 4
  end

  # Bit Gravity
  ForgetGravity    =  0
  NorthWestGravity =  1
  NorthGravity     =  2
  NorthEastGravity =  3
  WestGravity      =  4
  CenterGravity    =  5
  EastGravity      =  6
  SouthWestGravity =  7
  SouthGravity     =  8
  SouthEastGravity =  9
  StaticGravity    = 10

  # definitions for initial window state
  WithdrawnState = 0 # for windows that are not mapped
  NormalState    = 1 # most applications want to start this way
  IconicState    = 3 # application wants to start as an icon

  # Used in SetInputFocus, GetInputFocus
  RevertToNone        = None
  RevertToPointerRoot = PointerRoot
  RevertToParent      = 2

  # Property types
  XA_PRIMARY             =  1_u64
  XA_SECONDARY           =  2_u64
  XA_ARC                 =  3_u64
  XA_ATOM                =  4_u64
  XA_BITMAP              =  5_u64
  XA_CARDINAL            =  6_u64
  XA_COLORMAP            =  7_u64
  XA_CURSOR              =  8_u64
  XA_CUT_BUFFER0         =  9_u64
  XA_CUT_BUFFER1         = 10_u64
  XA_CUT_BUFFER2         = 11_u64
  XA_CUT_BUFFER3         = 12_u64
  XA_CUT_BUFFER4         = 13_u64
  XA_CUT_BUFFER5         = 14_u64
  XA_CUT_BUFFER6         = 15_u64
  XA_CUT_BUFFER7         = 16_u64
  XA_DRAWABLE            = 17_u64
  XA_FONT                = 18_u64
  XA_INTEGER             = 19_u64
  XA_PIXMAP              = 20_u64
  XA_POINT               = 21_u64
  XA_RECTANGLE           = 22_u64
  XA_RESOURCE_MANAGER    = 23_u64
  XA_RGB_COLOR_MAP       = 24_u64
  XA_RGB_BEST_MAP        = 25_u64
  XA_RGB_BLUE_MAP        = 26_u64
  XA_RGB_DEFAULT_MAP     = 27_u64
  XA_RGB_GRAY_MAP        = 28_u64
  XA_RGB_GREEN_MAP       = 29_u64
  XA_RGB_RED_MAP         = 30_u64
  XA_STRING              = 31_u64
  XA_VISUALID            = 32_u64
  XA_WINDOW              = 33_u64
  XA_WM_COMMAND          = 34_u64
  XA_WM_HINTS            = 35_u64
  XA_WM_CLIENT_MACHINE   = 36_u64
  XA_WM_ICON_NAME        = 37_u64
  XA_WM_ICON_SIZE        = 38_u64
  XA_WM_NAME             = 39_u64
  XA_WM_NORMAL_HINTS     = 40_u64
  XA_WM_SIZE_HINTS       = 41_u64
  XA_WM_ZOOM_HINTS       = 42_u64
  XA_MIN_SPACE           = 43_u64
  XA_NORM_SPACE          = 44_u64
  XA_MAX_SPACE           = 45_u64
  XA_END_SPACE           = 46_u64
  XA_SUPERSCRIPT_X       = 47_u64
  XA_SUPERSCRIPT_Y       = 48_u64
  XA_SUBSCRIPT_X         = 49_u64
  XA_SUBSCRIPT_Y         = 50_u64
  XA_UNDERLINE_POSITION  = 51_u64
  XA_UNDERLINE_THICKNESS = 52_u64
  XA_STRIKEOUT_ASCENT    = 53_u64
  XA_STRIKEOUT_DESCENT   = 54_u64
  XA_ITALIC_ANGLE        = 55_u64
  XA_X_HEIGHT            = 56_u64
  XA_QUAD_WIDTH          = 57_u64
  XA_WEIGHT              = 58_u64
  XA_POINT_SIZE          = 59_u64
  XA_RESOLUTION          = 60_u64
  XA_COPYRIGHT           = 61_u64
  XA_NOTICE              = 62_u64
  XA_FONT_NAME           = 63_u64
  XA_FAMILY_NAME         = 64_u64
  XA_FULL_NAME           = 65_u64
  XA_CAP_HEIGHT          = 66_u64
  XA_WM_CLASS            = 67_u64
  XA_WM_TRANSIENT_FOR    = 68_u64

  struct Depth
    depth : LibC::Int
    nvisuals : LibC::Int
    visuals : Visual*
  end

  struct ExtData
    number : Int32
    next : Pointer(ExtData)
    free_private : (Pointer(ExtData) -> Int32)
    private_data : XPointer
  end

  struct Visual
    ext_data : ExtData*
    visualid : LibC::ULong
    c_class : LibC::Int
    red_mask, green_mask, blue_mask : LibC::ULong
    bits_per_rgb : LibC::Int
    map_entries : LibC::Int
  end

  struct GC
    ext_data : ExtData*
    gid : GContext
  end

  struct ScreenFormat
    ext_data : ExtData*
    depth : LibC::Int
    bits_per_pixel : LibC::Int
    scanline_pad : LibC::Int
  end

  struct Screen
    ext_data : ExtData*
    display : Display*
    root : RootWindow
    width, height : LibC::Int
    mwidth, mheight : LibC::Int
    ndepths : LibC::Int
    depths : Depth*
    root_depth : LibC::Int
    root_visual : Visual*
    default_gc : GC
    cmap : Colormap
    white_pixel : LibC::ULong
    black_pixel : LibC::ULong
    max_maps, min_maps : LibC::Int
    backing_store : LibC::Int
    save_unders : Bool
    root_input_mask : LibC::Long
  end

  struct Display
    ext_data : ExtData*
    private1 : LibC::Char*
    fd : LibC::Int
    private2 : LibC::Int
    proto_major_version : LibC::Int
    proto_minor_version : LibC::Int
    vendor : LibC::Char*
    private3 : LibC::ULong
    private4 : LibC::ULong
    private5 : LibC::ULong
    private6 : LibC::Int
    resource_alloc : Display* -> LibC::ULong
    byte_order : LibC::Int
    bitmap_unit : LibC::Int
    bitmap_pad : LibC::Int
    bitmap_bit_order : LibC::Int
    nformats : LibC::Int
    pixmap_format : ScreenFormat*
    private8 : LibC::Int
    release : LibC::Int
    private9, private10 : LibC::Char*
    qlen : LibC::Int
    last_request_read : LibC::ULong
    request : LibC::ULong
    private11 : XPointer
    private12 : XPointer
    private13 : XPointer
    private14 : XPointer
    max_request_size : LibC::UInt
    db : LibC::Char*
    private15 : Display* -> LibC::Int
    display_name : LibC::Char*
    default_screen : LibC::Int
    nscreens : LibC::Int
    screens : Screen*
    motion_buffer : LibC::ULong
    private16 : LibC::ULong
    min_keycode : LibC::Int
    max_keycode : LibC::Int
    private17 : XPointer
    private18 : XPointer
    private19 : LibC::Int
    xdefaults : LibC::Char*
  end

  struct XWindowAttributes
    x, y : LibC::Int
    width, height : LibC::Int
    border_width : LibC::Int
    depth : LibC::Int
    visual : Visual*
    root : RootWindow
    class : LibC::Int
    bit_gravity : LibC::Int
    win_gravity : LibC::Int
    backing_store : LibC::Int
    backing_planes : LibC::ULong
    backing_pixel : LibC::ULong
    save_under : Bool
    colormap : Colormap
    map_installed : Bool
    map_state : LibC::Int
    all_event_masks : LibC::ULong
    your_event_mask : LibC::ULong
    do_not_propagate_mask : LibC::ULong
    override_redirect : Bool
    screen : Screen*
  end

  struct XWindowChanges
    x, y : LibC::Int
    width, height : LibC::Int
    border_width : LibC::Int
    sibling : Window
    stack_mode : StackMode
  end

  struct SizeHints
    flags : LibC::Long        # marks which fields in this structure are defined
    x, y : LibC::Int          # obsolete for new window mgrs, but clients
    width, height : LibC::Int # should set so old wm's don't mess up
    min_width, min_height : LibC::Int
    max_width, max_height : LibC::Int
    width_inc, height_inc : LibC::Int
    min_aspect, max_aspect : SizeHint_Aspect
    base_width, base_height : LibC::Int # added by ICCCM version 1
    win_gravity : WinGravity            # added by ICCCM version 1
  end

  struct SizeHint_Aspect
    x : LibC::Int # numerator
    y : LibC::Int # denominator
  end

  fun default_root_window = XDefaultRootWindow(
    display : Display*
  ) : RootWindow

  fun open_display = XOpenDisplay(
    display_name : LibC::Char*
  ) : Display*

  fun close_display = XCloseDisplay(
    dpy : Display*
  ) : LibC::Int

  fun free = XFree(x0 : Void*) : LibC::Int

  fun flush = XFlush(display : Display*) : LibC::Int

  fun get_input_focus = XGetInputFocus(
    display : Display*,
    focus_return : Window*,
    revert_to_return : LibC::Int*
  ) : Status

  fun get_window_property = XGetWindowProperty(
    display : Display*,
    w : Window,
    property : Atom,
    long_offset : LibC::Long,
    long_length : LibC::Long,
    delete : Bool,
    req_type : Atom,
    actual_type_return : Atom*,
    actual_format_return : LibC::Int*,
    nitems_return : LibC::ULong*,
    bytes_after_return : LibC::ULong*,
    prop_return : LibC::UChar**
  ) : LibC::Int

  fun intern_atom = XInternAtom(
    display : Display*,
    atom_name : LibC::UChar*,
    only_if_exists : LibC::Int
  ) : Atom

  fun get_atom_name = XGetAtomName(
    display : Display*,
    atom : Atom
  ) : LibC::UChar*

  fun move_window = XMoveWindow(
    display : Display*,
    w : Window,
    x : LibC::Int,
    y : LibC::Int
  )

  fun resize_window = XResizeWindow(
    display : Display*,
    w : Window,
    width : LibC::UInt,
    height : LibC::UInt
  )

  fun raise_window = XRaiseWindow(
    display : Display*,
    w : Window
  )

  fun lower_window = XLowerWindow(
    display : Display*,
    w : Window
  )

  fun get_window_attributes = XGetWindowAttributes(
    display : Display*,
    w : Window,
    window_attributes_return : XWindowAttributes*
  ) : Status

  fun configure_window = XConfigureWindow(
    display : Display*,
    w : Window,
    value_mask : LibC::UInt,
    values : XWindowChanges*
  ) : Status

  fun translate_coordinates = XTranslateCoordinates(
    display : Display*,
    src_w : Window,
    dest_w : Window,
    src_x : LibC::Int,
    src_y : LibC::Int,
    dest_x_return : LibC::Int*,
    dest_y_return : LibC::Int*,
    child_return : Window*
  ) : Bool

  fun get_geometry = XGetGeometry(
    display : Display*,
    d : Drawable,
    root_return : RootWindow*,
    x_return : LibC::Int*,
    y_return : LibC::Int*,
    width_return : LibC::UInt*,
    height_return : LibC::UInt*,
    border_width_return : LibC::UInt*,
    depth_return : LibC::UInt*
  ) : Bool

  fun set_input_focus = XSetInputFocus(
    display : Display*,
    focus : Window,
    revert_to : LibC::Int,
    time : Time
  ) : Status

  fun warp_pointer = XWarpPointer(
    display : Display*,
    src_w : Window,
    dest_w : Window,
    src_x : LibC::Int,
    src_y : LibC::Int,
    src_width : LibC::UInt,
    src_height : LibC::UInt,
    dest_x : LibC::Int,
    dest_y : LibC::Int
  ) : LibC::Int

  fun query_pointer = XQueryPointer(
    display : Display*,
    w : Window,
    root_return : Window*,
    child_return : Window*,
    root_x_return : Int32*,
    root_y_return : Int32*,
    win_x_return : Int32*,
    win_y_return : Int32*,
    mask_return : UInt32*
  ) : Bool

  fun get_wm_normal_hints = XGetWMNormalHints(
    display : Display*,
    w : Window,
    hints_return : SizeHints*,
    supplied_return : LibC::Long*
  ) : Status
end
