module WMHelper::X11
  record WindowProperty,
    type : LibX11::Atom,
    format : Int32,
    nitems : UInt64,
    data : LibC::UChar* do
  end

  def self.get_property_data(
    dpy : LibX11::Display*,
    window_id : LibX11::Window,
    atom_name : String
  ) : WindowProperty
    atom = LibX11.intern_atom(dpy, atom_name, true)

    return WindowProperty.new(
      type: 0_u64,
      format: 0,
      nitems: 0_u64,
      # TODO: There is probably a better way:
      data: StaticArray(UInt8, 1).new(0).to_unsafe
    ) if atom == LibX11::None

    status = LibX11.get_window_property(
      dpy, window_id, atom, 0, Int32::MAX, false, LibX11::AnyPropertyType,
      out actual_type, out actual_format, out nitems, out bytes_after, out data
    )

    # puts "============"
    # puts "atom: #{atom_name}"
    # puts "actual_type: #{actual_type}"
    # puts "actual_format: #{actual_format}"
    # puts "nitems: #{nitems}"
    # puts "bytes_after: #{bytes_after}"
    # puts "------------"

    WindowProperty.new(
      type: actual_type,
      format: actual_format,
      nitems: nitems,
      data: data
    )
  end

  def self.get_atom_name(dpy : LibX11::Display*, type_id) : String
    return "" if type_id == 0

    atom_name_p = LibX11.get_atom_name(dpy, type_id)
    atom_name = String.new(atom_name_p)
    LibX11.free atom_name_p
    return atom_name
  end

  # TODO: Implement support for encoded strings (COMPOUND_TEXT, UTF8_STRING)
  # TODO: Implement support for array of strings, used in WM_CLASS (low prio)
  def self.get_string_property(
    dpy : LibX11::Display*,
    window_id : LibX11::Window,
    atom_name : String
  ) : String
    prop = get_property_data(dpy, window_id, atom_name)
    # TODO: Is this the right way to check for null?
    return "" if prop.data == Pointer(UInt8).null

    str = String.new(prop.data.as(UInt8*))
    LibX11.free prop.data
    return str
  end

  def self.get_property(
    dpy : LibX11::Display*,
    window_id : LibX11::Window,
    atom_name : String
  ) : Array(UInt64)
    prop = get_property_data(dpy, window_id, atom_name)

    return [] of UInt64 if prop.type == 0 || prop.nitems == 0

    value = prop.data.as(UInt64*)
    data_array = [] of UInt64
    (0..prop.nitems - 1).each do |idx|
      data_array << value[idx]
    end

    LibX11.free prop.data

    return data_array
  end
end
