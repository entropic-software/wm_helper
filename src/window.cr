require "./libx11.cr"
require "./libxinerama.cr"
require "./properties.cr"
require "./wm.cr"
require "./wm_helper.cr"
require "./x11.cr"

class WMHelper::Window
  property id : LibX11::Window
  property stacking : Int32 # Lower stacking number means higher up in the stack
  # TODO: update screen after move / resize, because win might have moved
  # across screen boundries
  property screen : LibXinerama::XineramaScreenInfo
  protected property internal_geometry : Geometry
  protected property geometry_with_frame : Geometry
  protected property gravity : LibX11::WinGravity
  protected property root : LibX11::RootWindow
  protected property dpy : Pointer(LibX11::Display)
  protected property screens : Array(LibXinerama::XineramaScreenInfo)

  def initialize(
    @id : LibX11::Window,
    @dpy : Pointer(LibX11::Display),
    @root : LibX11::RootWindow,
    @screens : Array(LibXinerama::XineramaScreenInfo),
    @stacking : Int32
  )
    @internal_geometry = get_internal_geometry
    @geometry_with_frame = get_geometry_with_frame
    @gravity = get_gravity
    @screen = on_screen(@screens)
  end

  def to_s
    {
      title: get_title,
      id:    @id,
      geo:   @internal_geometry,
      frame: get_geometry_with_frame,
      grav:  @gravity,
      stack: @stacking,
    }
  end

  ##########################################
  # External interface.
  # It always include frame, because it's intended for other windows.

  def geometry : Geometry
    @geometry_with_frame
  end

  def right : Int32
    @geometry_with_frame.x + @geometry_with_frame.width
  end

  def left : Int32
    @geometry_with_frame.x
  end

  def bottom : Int32
    @geometry_with_frame.y + @geometry_with_frame.height
  end

  def top : Int32
    @geometry_with_frame.y
  end

  def width : Int32
    @geometry_with_frame.width
  end

  def height : Int32
    @geometry_with_frame.height
  end

  def is_hidden?
    Properties.get_wm_state(@dpy, @id).each do |val|
      # TODO: Ensure this is correct. I've got it from experimentation:
      return true if val == LibX11::IconicState
    end

    Properties.get_net_wm_state(@dpy, @id).each do |val|
      return true if Properties.get_atom_name(@dpy, val) == "_NET_WM_STATE_HIDDEN"
    end

    return false
  end

  def ignore_focus?
    attributes = [
      "_NET_WM_STATE_BELOW",
      "_NET_WM_STATE_SKIP_PAGER",
      "_NET_WM_STATE_SKIP_TASKBAR",
    ]

    Properties.get_net_wm_state(@dpy, @id).each do |val|
      devel_puts "IGNORED via ignore_focus: " + get_title
      return true if attributes.includes?(Properties.get_atom_name(@dpy, val))
    end

    return false
  end

  def get_title : String
    prop_value = Properties.get_wm_name(@dpy, @id)
    return "UNNAMED id: #{@id}" if prop_value.empty?
    prop_value
  end

  # The desktop this window is on
  def get_desktop : Int64
    prop_values = Properties.get_net_wm_desktop(@dpy, @id)
    return DesktopNone if prop_values.size != 1

    val : UInt64 = prop_values[0]
    if val == UInt64::MAX
      # UInt64::MAX represents -1 in Int64, which means "sticky"
      DesktopSticky
    else
      val.to_i64
    end
  end

  ##########################################
  # Internal

  protected def get_gravity : LibX11::WinGravity
    get_size_hints.win_gravity
  end

  protected def get_size_hints : LibX11::SizeHints
    X11.get_wm_normal_hints(@dpy, @id)
  end

  # The Xinerama screen this window is on
  protected def on_screen(
    screens : Array(LibXinerama::XineramaScreenInfo)
  ) : LibXinerama::XineramaScreenInfo
    default_screen : LibXinerama::XineramaScreenInfo = screens[0]
    screens.find(default_screen) { |scr|
      between?(get_center.x, scr.x_org, scr.x_org + scr.width - 1)
    }
  end

  protected def get_frame_extents : FrameExtents
    attrs = X11.get_window_attributes(@dpy, @id)
    border_width = attrs.border_width

    frame = Properties.get_net_frame_extents(@dpy, @id)
    return FrameExtents.new(0, 0, 0, 0) + border_width if frame.empty?
    raise "Invalid _NET_FRAME_EXTENTS" if frame.size != 4

    return FrameExtents.new(
      left: frame[0],
      right: frame[1],
      top: frame[2],
      bottom: frame[3]
    ) + border_width
  end

  # Includes the window manager's frame
  # Top-left position of the windowmanager's frame, not the window interior
  # Relative to display
  protected def get_geometry_with_frame : Geometry
    frame_extents = get_frame_extents
    return Geometry.new(
      x: @internal_geometry.x - frame_extents.left,
      y: @internal_geometry.y - frame_extents.top,
      width: (@internal_geometry.width + frame_extents.left + frame_extents.right).to_i32,
      height: (@internal_geometry.height + frame_extents.top + frame_extents.bottom).to_i32,
    )
  end

  # Does not include the window manager's frame
  # Top-left position of the window interior
  # Relative to display
  protected def get_internal_geometry : Geometry
    size = X11.get_size(@dpy, @id)
    pos = X11.get_position(@dpy, @id, @root)

    return Geometry.new(
      x: pos.x,
      y: pos.y,
      width: size.width,
      height: size.height,
    )
  end

  # Coordinate of the center of the window, relative to display
  def get_center : Position
    Position.new(
      x: @internal_geometry.x + (@internal_geometry.width // 2),
      y: @internal_geometry.y + (@internal_geometry.height // 2)
    )
  end

  def to_top
    LibX11.raise_window(@dpy, @id)
  end

  def to_bottom
    LibX11.lower_window(@dpy, @id)
  end

  def pack_to(direction : Direction, other : Window)
    case direction
    in Direction::Left
      move Position.new(other.right, self.top)
    in Direction::Right
      move Position.new(other.left - self.width, self.top)
    in Direction::Up
      move Position.new(self.left, other.bottom)
    in Direction::Down
      move Position.new(self.left, other.top - self.height)
    in Direction::None
    end
  end

  def move_to_screen_edge(direction : Direction)
    case direction
    in Direction::Left
      move Position.new(@screen.x_org, self.top)
    in Direction::Right
      move Position.new(@screen.x_org + @screen.width - self.width, self.top)
    in Direction::Up
      move Position.new(self.left, @screen.y_org)
    in Direction::Down
      move Position.new(self.left, @screen.y_org + @screen.height - self.height)
    in Direction::None
    end
  end

  protected def move(to_pos : Position)
    move_and_or_resize(
      Geometry.new(
        x: to_pos.x,
        y: to_pos.y,
        width: self.width,
        height: self.height
      )
    )
  end

  protected def resize(new_size : Size)
    move_and_or_resize(
      Geometry.new(
        x: self.left,
        y: self.top,
        width: new_size.width,
        height: new_size.height
      )
    )
  end

  # TODO: If a window has size steps (SizeHints.width_inc, SizeHints.height_inc)
  # consider these (in addition to base_width and base_height) when calculating
  # new size/pos
  # TODO: Take aspect_ratio into account when resizing.
  # Note that `new_geo` includes the frame
  def move_and_or_resize(new_geo : Geometry)
    return if new_geo == @geometry_with_frame

    old_geo = @internal_geometry

    frame_extents = get_frame_extents
    size_hints = get_size_hints
    display = WM.display_size(@screens)

    # Depending on gravity, use frame size when calculating new size
    if get_gravity == LibX11::StaticGravity
      x = new_geo.x + frame_extents.left
      y = new_geo.y + frame_extents.top
    else
      x = new_geo.x
      y = new_geo.y
    end

    width = new_geo.width - frame_extents.left - frame_extents.right
    height = new_geo.height - frame_extents.top - frame_extents.bottom

    # hints max can be zero, in that case use screen size
    hints_max_w = if size_hints.max_width < 1
                    screen.width
                  else
                    size_hints.max_width
                  end
    hints_max_h = if size_hints.max_height < 1
                    screen.height
                  else
                    size_hints.max_height
                  end

    devel_puts "display: #{display}"
    devel_puts "move/resize from #{@internal_geometry} to #{new_geo}"

    values = LibX11::XWindowChanges.new(
      stack_mode: LibX11::StackMode::Above,
      x: x.clamp(0, display.width),
      y: y.clamp(0, display.height),
      width: width
        .clamp(size_hints.min_width, hints_max_w)
        .clamp(0, screen.width),
      height: height
        .clamp(size_hints.min_height, hints_max_h)
        .clamp(0, screen.height),
    )

    configure(
      LibX11::CWX | LibX11::CWY | LibX11::CWWidth | LibX11::CWHeight | LibX11::CWStackMode,
      values
    )

    wait_for_geometry_update(old_geo)
  end

  protected def configure(value_mask, values)
    devel_puts "configure values: #{values}"
    X11.configure_window(@dpy, @id, value_mask, values)
  end

  protected def wait_for_geometry_update(old_geo : Geometry)
    max_tries = 10
    i = 0
    loop do
      test_geo = get_internal_geometry
      is_updated = (test_geo != old_geo)
      break if is_updated || i > max_tries

      sleep 0.03
      i += 1
    end

    @internal_geometry = get_internal_geometry
    @geometry_with_frame = get_geometry_with_frame
    @gravity = get_gravity
  end

  def take_focus
    LibX11.set_input_focus(@dpy, @id, LibX11::RevertToParent, LibX11::CurrentTime)
  end
end
